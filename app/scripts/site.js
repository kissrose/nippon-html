/**
 * @name Site
 * @description Global variables and functions
 * @version 1.0
 */

var Site = (function($, window, undefined) {
  'use strict';
  
    //Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();

})(jQuery, window);

var edgtfGlobalVars = {"vars":{"edgtfAddForAdminBar":0,"edgtfElementAppearAmount":-50,"edgtfFinishedMessage":"No more posts","edgtfMessage":"Loading new posts...","edgtfTopBarHeight":0,"edgtfStickyHeaderHeight":0,"edgtfStickyHeaderTransparencyHeight":60,"edgtfLogoAreaHeight":0,"edgtfMenuAreaHeight":88,"edgtfFixedAreaHeight":60,"edgtfInitialMenuAreaHeight":88}};
var edgtfPerPageVars = {"vars":{"edgtfStickyScrollAmount":0,"edgtfHeaderTransparencyHeight":88}};
var a = jQuery;
var edgtf = {};

edgtf.window = a(window);
edgtf.document = a(document);
edgtf.windowWidth = a(window).width();
edgtf.windowHeight = a(window).height();
edgtf.body = a("body");
edgtf.html = a("html, body");

function k() {
    var b, c, d, e, f, g, h, i, j = a(".edgtf-circle-carousel");
    j.length && j.each(function() {
        function j(a) {
            if (!l.hasClass("edgtf-circle-carousel-navigation-disabled")) {
                var b, c, d, e;
                d = m.children().length, e = m.children().index(a), d > 1 ? (c = 0 !== e ? e : d, b = e !== d - 1 ? d - e - 1 : d) : c = b = 0, k(n.find(".edgtf-prev-icon-holder .edgtf-navigation-counter"), c), k(n.find(".edgtf-next-icon-holder .edgtf-navigation-counter"), b)
            }
        }

        function k(a, b) {
            n.find(a).text(b)
        }
        var l = a(this),
            m = l.children(".edgtf-circle-carousel-slider"),
            n = l.children(".edgtf-circle-carousel-navigation");
        if (l.appear(function() {
                l.css("visibility", "visible")
            }, {
                accX: 0,
                accY: edgtfGlobalVars.vars.edgtfElementAppearAmount
            }), 
            
            b = "" !== l.data("height") && void 0 !== l.data("height") ? l.data("height") : 480, c = "" !== l.data("autoplay") && void 0 !== l.data("autoplay") ? l.data("autoplay") : 3e3, d = "" !== l.data("speed") && void 0 !== l.data("speed") ? l.data("speed") : 800, e = "" !== l.data("separation") && void 0 !== l.data("separation") ? l.data("separation") : 425, f = "" !== l.data("flankingitems") && void 0 !== l.data("flankingitems") ? l.data("flankingitems") : 1, g = "" !== l.data("edgefadeenabled") && void 0 !== l.data("edgefadeenabled") ? l.data("edgefadeenabled") : !1, h = "" !== l.data("sizemultiplier") && void 0 !== l.data("sizemultiplier") ? l.data("sizemultiplier") : .7, i = "no" === l.data("navigation") ? "edgtf-circle-carousel-navigation-disabled" : "", l.addClass(i), m.css({
                height: b
            }), edgtf.windowWidth < 1457 && edgtf.windowWidth > 1200 ? e > 325 && (e = 325) : edgtf.windowWidth < 1217 && edgtf.windowWidth > 1024 ? e = 250 : edgtf.windowWidth < 1025 && edgtf.windowWidth > 768 ? e = 220 : edgtf.windowWidth < 769 && edgtf.windowWidth > 600 ? (e = 180, m.css({
                height: .9 * b
            })) : edgtf.windowWidth < 601 && edgtf.windowWidth > 480 ? (e = 160, m.css({
                height: .8 * b
            })) : edgtf.windowWidth < 481 && (e = 100, m.css({
                height: .7 * b
            })), 
            
            m.waterwheelCarousel({
                autoPlay: c,
                speed: d,
                flankingItems: f,
                separation: e,
                opacityMultiplier: 0.5,
                edgeFadeEnabled: g,
                sizeMultiplier: h,
                preloadImages: !0,
                activeClassName: "edgtf-circle-carousel-active",
                movingToCenter: function(a) {
                    j(a)
                }
            }), n.find(".edgtf-circle-carousel-icon.arrow_left").on("click", function(a) {
                a.preventDefault(), m.prev()
            }), n.find(".edgtf-circle-carousel-icon.arrow_right").on("click", function(a) {
                a.preventDefault(), m.next()
            }), !l.hasClass("edgtf-circle-carousel-init") && !l.hasClass("edgtf-circle-carousel-navigation-disabled")) {
            l.addClass("edgtf-circle-carousel-init");
            
            var o = m.children().length - 1;
            k(n.find(".edgtf-prev-icon-holder .edgtf-navigation-counter"), 0), k(n.find(".edgtf-next-icon-holder .edgtf-navigation-counter"), o)
        }
    })
}

k();

/*
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=696539990376522";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
*/

$('.home-slider .slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  // fade: true,
  asNavFor: '.home-slider .slider-nav'
});
$('.home-slider .slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.home-slider .slider-for',
  // dots: true,
  centerMode: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

$(document).ready(function(){
  $('#contact-list .panel-heading').click(function() {
    var that = $(this);
    
    var item = that.parent().find('.panel-collapse');
    item.slideToggle();
    
    $('#contact-list').find('.panel-collapse').not(item).slideUp();
  });
  
  /**
   * Navbar Click
   */
  $('#navbar > ul > li > a').click(function(){
	 var that = $(this);
	 
	 var itemAnchor = that.attr('href');
	 
	 if (!$(itemAnchor).length) {
		 return false;
	 }
	 
	 $('html, body').animate({
        scrollTop: $(itemAnchor).offset().top - 100
    }, 800);
	 
	 return true;
  });
  
  var $goToTop = $('.gototop'),
	$window = $(window),
	scrollOffsetFromTop = 600;
  
  $(window).on('resize scroll', function() {
	if($window.scrollTop() > scrollOffsetFromTop){
		$goToTop.fadeIn("slow");
	} else{
		$goToTop.fadeOut("slow");
	}
  });
  
  $goToTop.on("click",function() {
		$('body,html').stop(true).animate({
			scrollTop:0}
		,1500);
		return false;
	});

  /**
   * Navbar Hover
   */
  $('#navbar > ul > li.dropdown').hover(function(){
    $(this).find('.dropdown-menu').slideToggle();
  });
  
  
  $('.no-touch .bwWrapper').BlackAndWhite({
	    hoverEffect : true, // default true
	    // set the path to BnWWorker.js for a superfast implementation
	    webworkerPath : false,
	    // to invert the hover effect
	    invertHoverEffect: false,
	    // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)
	    intensity:1,
	    speed: { //this property could also be just speed: value for both fadeIn and fadeOut
	        fadeIn: 50, // 200ms for fadeIn animations
	        fadeOut: 100 // 800ms for fadeOut animations
	    },
	    onImageReady:function(img) {
	        // this callback gets executed anytime an image is converted
	    }
	});

  $('.horizontal-scrollpane').jScrollPane({
    horizontalDragMinWidth : 40,
    horizontalDragMaxWidth : 40,
    maintainPosition : false
  });
  $('.jspVerticalBar').hide();  
});

haccordion.setup({
  accordionid: 'room-accordion', //main accordion div id
  paneldimensions: {
    peekw:'120px',
    fullw:'570px',
    h:'455px'
  },
  selectedli: [0, true], //[selectedli_index, persiststate_bool]
  collapsecurrent: false //collapse current expanded li when mouseout into general space?
})

function initializeMap() {
	  var mapCanvas = document.getElementById('distributor-map');
	  var mapOptions = {
	    center: new google.maps.LatLng(10.7842373,106.6679003),
	    zoom: 8,
	    scrollwheel: false,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  }
	  var map = new google.maps.Map(mapCanvas, mapOptions)
}
//google.maps.event.addDomListener(window, 'load', initializeMap);


function initializeMap2() {
  var mapCanvas = document.getElementById('contact-map');
  var mapOptions = {
    center: new google.maps.LatLng(10.7842373,106.6679003),
    zoom: 8,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(mapCanvas, mapOptions)
}
//google.maps.event.addDomListener(window, 'load', initializeMap2);
